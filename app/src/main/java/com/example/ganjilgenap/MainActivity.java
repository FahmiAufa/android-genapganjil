package com.example.ganjilgenap;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView txtResult;
    ListView lvResult;
    EditText edtBilangan;
    String[] daftar;
    String[] id;
    Button btnSubmit;
    DataHelper dbcenter;
    protected Cursor cursor;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtBilangan = (EditText) findViewById(R.id.edtBilangan);
        txtResult = (TextView)findViewById(R.id.txtResult);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        dbcenter = new DataHelper(this);
        RefreshList();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtBilangan.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Harus Memasukan Bilangan", Toast.LENGTH_LONG).show();
                }else {
//                    int n1 = Integer.parseInt(edtBilangan.getText().toString()) ;
////                    for (int i = 0; i <=n1; i++) {
//                    if(n1 % 2==0){
//                        txtResult.setText("Genap");
//                    }else{
//                        txtResult.setText("Ganjil");
//                    }
//                        txtResult.setText(BilanganPrima());
//                    }
                        String bilanganFibonaci = "";
                        for (int i = 0; i <= Integer.parseInt(edtBilangan.getText().toString()); i++) {
                            BilanganFibonaci(i);
                            bilanganFibonaci = bilanganFibonaci + BilanganFibonaci(i) + " ";
                        }
                        txtResult.setText(bilanganFibonaci);
//                    SQLiteDatabase db = dbcenter.getWritableDatabase();
//                    db.execSQL("insert into tbl_mst_hasil(bilangan, hasil) values('" +
//                            edtBilangan.getText().toString() + "','" +
//                            txtResult.getText().toString() + "')");
//                    Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
//                    RefreshList();
//                finish();
//                edtBilangan.setText(null);
                }
            }

        });
    }

    private long BilanganFibonaci(long angka) {
        if((angka == 0) || (angka == 1)){
            return angka;
        }else {
            return BilanganFibonaci(angka - 1) + BilanganFibonaci(angka - 2);
        }
    }

    private String BilanganPrima() {
        int index = 0;
        int angka = 0;

        String bilanganPrima = "";

        for(index = 1; index <= Integer.parseInt(edtBilangan.getText().toString()); index++){
            int counter = 0;

            for(angka = index; angka >=1; angka = angka -1){
                if(index % angka == 0){
                    counter = counter + 1;
                 }
            }
            if (counter == 2){
                bilanganPrima = bilanganPrima + index + " ";
            }
        }
        return bilanganPrima;
    }

    private void RefreshList() {
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM tbl_mst_hasil",null);
        daftar = new String[cursor.getCount()];
        id = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int cc=0; cc < cursor.getCount(); cc++){
            cursor.moveToPosition(cc);
            daftar[cc] = cursor.getString(1).toString() + " adalah bilangan " + cursor.getString(2).toString();
            id[cc] = cursor.getString(0).toString();
        }
        lvResult = (ListView)findViewById(R.id.lvResult);
        lvResult.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, daftar));
        lvResult.setSelected(true);
        lvResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final String selection = id[i];
                final CharSequence[] dialogitem = {"Hapus Bilangan"};
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch(item){
                            case 0 :
                                SQLiteDatabase db = dbcenter.getWritableDatabase();
                                db.execSQL("delete from tbl_mst_hasil where no = '"+selection+"'");
                                RefreshList();
                                break;
                        }
                    }
                });
                builder.create().show();
            }
        });
        ((ArrayAdapter)lvResult.getAdapter()).notifyDataSetInvalidated();
    }
}